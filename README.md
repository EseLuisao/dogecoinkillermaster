# Dogecoinkillermaster 🤖

A responsive web site used to publish nft pixel art designs.
### Technologies 💻
- Javascript 🚀
- HTML 📰
- CSS 🎨
- Fontawesome (icons library) 🅰️
- Howler (audio library) 🎶

## Index
[![N|Solid](https://gitlab.com/EseLuisao/dogecoinkillermaster/-/raw/main/images/index.jpeg)](https://gitlab.com/EseLuisao/dogecoinkillermaster/-/raw/main/images/index.jpeg)

## Roadmap
[![N|Solid](https://gitlab.com/EseLuisao/dogecoinkillermaster/-/raw/main/images/roadmap.jpeg)](https://gitlab.com/EseLuisao/dogecoinkillermaster/-/raw/main/images/roadmap.jpeg)

## Gallery
[![N|Solid](https://gitlab.com/EseLuisao/dogecoinkillermaster/-/raw/main/images/gallery.jpeg)](https://gitlab.com/EseLuisao/dogecoinkillermaster/-/raw/main/images/gallery.jpeg)

## Gallery Mobile
[![N|Solid](https://gitlab.com/EseLuisao/dogecoinkillermaster/-/raw/main/images/gallery%20mobile.jpeg)](https://gitlab.com/EseLuisao/dogecoinkillermaster/-/raw/main/images/gallery%20mobile.jpeg)
